package com.example.venkat.viewflipper;

import android.app.ActionBar;
import android.net.http.AndroidHttpClient;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private ViewFlipper mViewFlipper1;
    private ViewFlipper mViewFlipper2;
    private ViewFlipper mViewFlipper3;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        mViewFlipper1 = (ViewFlipper) v.findViewById(R.id.viewFlipper1);
        mViewFlipper2 = (ViewFlipper) v.findViewById(R.id.viewFlipper2);
        mViewFlipper3 = (ViewFlipper) v.findViewById(R.id.viewFlipper3);
        MovieDataJsonLocal mvData = new MovieDataJsonLocal();
        mvData.loadLocalMovieDataJson(getActivity());
        int i = 0;
        while (i < mvData.getSize() - 3) {
            LinearLayout.LayoutParams flipper_layout_params = new LinearLayout.LayoutParams(250, 600);
            LinearLayout flipper1ImgesLayout = new LinearLayout(getActivity());
            flipper1ImgesLayout.setOrientation(LinearLayout.HORIZONTAL);
            flipper1ImgesLayout.setLayoutParams(flipper_layout_params);

            LinearLayout flipper2ImgesLayout = new LinearLayout(getActivity());
            flipper2ImgesLayout.setOrientation(LinearLayout.HORIZONTAL);
            flipper2ImgesLayout.setLayoutParams(flipper_layout_params);

            LinearLayout flipper3ImgesLayout = new LinearLayout(getActivity());
            flipper3ImgesLayout.setOrientation(LinearLayout.HORIZONTAL);
            flipper3ImgesLayout.setLayoutParams(flipper_layout_params);

            ImageView img1 = new ImageView(getActivity());
            ImageView img2 = new ImageView(getActivity());
            ImageView img3 = new ImageView(getActivity());
            LinearLayout.LayoutParams imgparams = new LinearLayout.LayoutParams(240, 400);
            img1.setLayoutParams(imgparams);
            img2.setLayoutParams(imgparams);
            img3.setLayoutParams(imgparams);
            img1.setImageResource((Integer) mvData.getItem(i).get("image"));
            img2.setImageResource((Integer) mvData.getItem(i + 1).get("image"));
            img3.setImageResource((Integer) mvData.getItem(i + 2).get("image"));
            flipper1ImgesLayout.addView(img1);
            flipper2ImgesLayout.addView(img2);
            flipper3ImgesLayout.addView(img3);
            mViewFlipper1.addView(flipper1ImgesLayout);
            mViewFlipper2.addView(flipper2ImgesLayout);
            mViewFlipper3.addView(flipper3ImgesLayout);
            i = i + 1;
        }
//        while (i < mvData.getSize() - 3) {
//            LinearLayout flipperImgesLayout = new LinearLayout(getActivity());
//            flipperImgesLayout.setOrientation(LinearLayout.HORIZONTAL);
//            LinearLayout.LayoutParams flipper_layout_params = new LinearLayout.LayoutParams(400,600);
//            ImageView img1 = new ImageView(getActivity());
//            ImageView img2 = new ImageView(getActivity());
//            ImageView img3 = new ImageView(getActivity());
//            LinearLayout.LayoutParams imgparams = new LinearLayout.LayoutParams(240, 400);
//            img1.setLayoutParams(imgparams);
//            img2.setLayoutParams(imgparams);
//            img3.setLayoutParams(imgparams);
//            img1.setImageResource((Integer) mvData.getItem(i).get("image"));
//            img2.setImageResource((Integer) mvData.getItem(i + 1).get("image"));
//            img3.setImageResource((Integer) mvData.getItem(i + 2).get("image"));
//            flipperImgesLayout.addView(img1);
//            flipperImgesLayout.addView(img2);
//            flipperImgesLayout.addView(img3);
//            mViewFlipper.addView(flipperImgesLayout);
//            i = i + 3;
//        }
        mViewFlipper1.setAutoStart(true);
        mViewFlipper1.setFlipInterval(4000);

        mViewFlipper2.setAutoStart(true);
        mViewFlipper2.setFlipInterval(4000);

        mViewFlipper3.setAutoStart(true);
        mViewFlipper3.setFlipInterval(4000);

        mViewFlipper1.startFlipping();
        mViewFlipper2.startFlipping();
        mViewFlipper3.startFlipping();

        //mViewFlipper.setInAnimation(getActivity(), android.R.anim.slide_in_left);
        mViewFlipper1.setInAnimation(getActivity(), R.animator.in_from_right);
        mViewFlipper1.setOutAnimation(getActivity(), R.animator.out_to_left);

        mViewFlipper2.setInAnimation(getActivity(), R.animator.in_from_right);
        mViewFlipper2.setOutAnimation(getActivity(), R.animator.out_to_left);

        mViewFlipper3.setInAnimation(getActivity(), R.animator.in_from_right);
        mViewFlipper3.setOutAnimation(getActivity(), R.animator.out_to_left);
        //mViewFlipper.setOutAnimation(getActivity(), android.R.anim.slide_out_right);
        return v;
    }
}
